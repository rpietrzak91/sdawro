//package okienko;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class rejestracja extends JFrame implements ActionListener {
	
	private JLabel podajImie,podajNazwisko,podajPesel,podajMiejscowosc,podajWiek,podajpelnotelnosc, podajRegulamin;
	//, podajWiek potPelnoletnosc, akcRegulamin;
	JTextField wprowadzImie, wprowadzNazwisko, wprowadzPesel, wprowadzMiejscowosc, wprowadzWiek;
	JCheckBox akceptPelnoletnosc, akceptRegulamin;
	JButton rejestracja;
	String imie,nazwisko,miejscowosc, pesel, miejscowosc1, wynikWiek;
	int wiek;
	double tempimie;
	JLabel oImie,oNazwisko,oPesel,oMiejscowosc,oWiek,oPelnoletnosc,oRegulamin;
	
	public rejestracja()
	{
		setSize(650,400);
		setTitle("Rejestracja");
		setLayout(null);
		
		podajImie = new JLabel("Imi�:");
		podajImie.setBounds(10,10,50,50);
		Font font = new Font("Sanserif",Font.BOLD, 14);
	    add(podajImie);
	    podajImie.setFont(font);
	    
	    wprowadzImie = new JTextField();
	    wprowadzImie.setBounds(120, 26, 150, 20);
	    add(wprowadzImie);
	    wprowadzImie.setToolTipText("Wprowadz Imi�");
	    wprowadzImie.addActionListener(this);
	    
	    
	    
		podajNazwisko = new JLabel("Nazwisko:");
		podajNazwisko.setBounds(10,40,70,70);
		add(podajNazwisko);
	    podajNazwisko.setFont(font);
	    
	    
	    wprowadzNazwisko = new JTextField();
	    wprowadzNazwisko.setBounds(120, 65, 150, 20);
	    add(wprowadzNazwisko);
	    wprowadzNazwisko.setToolTipText("Wprowadz Nazwisko");
	    wprowadzNazwisko.addActionListener(this);
		
	    podajPesel = new JLabel("Pesel:");
		podajPesel.setBounds(10,80,70,70);
		add(podajPesel);
	    podajPesel.setFont(font);
	    
	    
	    wprowadzPesel = new JTextField();
	    wprowadzPesel.setBounds(120, 104, 150, 20);
	    add(wprowadzPesel);
	    wprowadzPesel.setToolTipText("Wprowadz Pesel");
	    wprowadzPesel.addActionListener(this);
	    
	    podajMiejscowosc = new JLabel("Miejscowosc:");
		podajMiejscowosc.setBounds(10,120,100,70);
		add(podajMiejscowosc);
	    podajMiejscowosc.setFont(font);
	    
	    wprowadzMiejscowosc = new JTextField();
	    wprowadzMiejscowosc.setBounds(120, 143, 150, 20);
	    add(wprowadzMiejscowosc);
	    wprowadzMiejscowosc.setToolTipText("Wprowadz Miejscowosc");
	    wprowadzMiejscowosc.addActionListener(this);
	    
	    podajWiek = new JLabel("Wiek:");
		podajWiek.setBounds(10,160,100,70);
		add(podajWiek);
	    podajWiek.setFont(font);
	    
	    
	    wprowadzWiek = new JTextField();
	    wprowadzWiek.setBounds(120, 182, 150, 20);
	    add(wprowadzWiek);
	    wprowadzWiek.setToolTipText("Wprowadz Wiek");
	    wprowadzWiek.addActionListener(this);
	    
	    akceptPelnoletnosc = new JCheckBox("");
	    akceptPelnoletnosc.setBounds(10,210,20,30);
		add(akceptPelnoletnosc);
		akceptPelnoletnosc.setFont(font);
		akceptPelnoletnosc.addActionListener(this);
		
		podajpelnotelnosc = new JLabel("O�wiadczam, �e mam 18 lat");
		podajpelnotelnosc.setBounds(45,214,250,20);
		add(podajpelnotelnosc);
		podajpelnotelnosc.setFont(font);
		
		
		akceptRegulamin = new JCheckBox("");
		akceptRegulamin.setBounds(10,240,20,30);
		add(akceptRegulamin);
		akceptRegulamin.setFont(font);
		akceptRegulamin.addActionListener(this);
		
		podajRegulamin = new JLabel("Zaakceptuj Regulamin");
		podajRegulamin.setBounds(45,244,250,20);
		add(podajRegulamin);
		podajRegulamin.setFont(font);
		
		rejestracja = new JButton("REJESTRACJA");
		rejestracja.setBounds(100,300,150,40);
		add(rejestracja);
		rejestracja.addActionListener(this);
		
	}
	
	
	
	

	public static void main(String[] args) {
		
		rejestracja ob1 = new rejestracja();
		ob1.setVisible(true);
		ob1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}





	@Override
	public void actionPerformed(ActionEvent g) {
		Object rej = g.getSource();
		boolean ok = false;
		boolean rOk = false;
		if(rej == rejestracja )
		{
			
			String odczytImie = wprowadzImie.getText();
			String odczytNazwisko = wprowadzNazwisko.getText();
			String odczytPesel = wprowadzPesel.getText();
			String odczytMiejscowosc = wprowadzMiejscowosc.getText();
			String odczytWiek = wprowadzWiek .getText();
			
			if(akceptPelnoletnosc.isSelected() == true)
			{
				ok = true;
				
			}
		
			if(akceptRegulamin.isSelected() == true)
			{
				rOk =true;
			}
		
			JOptionPane.showMessageDialog(null, "Imi�: " + odczytImie + "\n" + "Nazwisko: " + odczytNazwisko + "\n" + "Pesel: " + odczytPesel + "\n" + "Miejscowosc: " + odczytMiejscowosc + "\n"+ "Wiek: " + odczytWiek + "\n" + "O�wiadczenie 18 lat: " + ok + "\n" + "Akceptacja Regulaminu: " + rOk);
			
			
		}
		
	
	}
	


}
