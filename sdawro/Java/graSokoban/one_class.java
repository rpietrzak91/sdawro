import java.applet.Applet;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TimerTask;

public class one_class extends Applet implements KeyListener {
	
	int rozmiar1 = 14*40;
	int rozmiar2 = 10*40;
	static Applet apki;
	Zadanie Zadanie;
	Zadanie obliczenia = new Zadanie();
	Timer timer = new Timer();
	Image bufor;
	Graphics bg;
	public void init()
	{
	
		apki = this;
		apki.addKeyListener(this);
		apki.setSize(rozmiar1, rozmiar2);
		apki.setBackground(Color.DARK_GRAY);
		bufor = createImage(rozmiar1, rozmiar2);
		bg = bufor.getGraphics();
		timer.scheduleAtFixedRate(obliczenia, 10, 10);
		obliczenia.zrobPlansze();

	}
	public void update(Graphics g)
	{
		
		bg.clearRect(0, 0, rozmiar1, rozmiar2);
		paint(bg);
		g.drawImage(bufor, 0, 0, apki);
	}
	public void paint(Graphics g)
	{
		
//		g.setColor(Color.RED);
//		g.fillRect(obliczenia.poz1, obliczenia.poz2, 100, 100);
    
//	    Font font = new Font("New Times Roman",Font.BOLD, 24);
//	    g.setFont(font);
//	    g.setColor(Color.BLUE);
//	    g.drawString("Raados�aw Pietrzak", 222, 100);
	    
		rysujPlansze(g);
		
		
	}
	public void rysujPlansze(Graphics g) {
		
		
		for(int i=0;i<obliczenia.plansza.length;i++)
		{
			for(int j=0; j<obliczenia.plansza[0].length;j++)
			{
				switch(obliczenia.plansza[i][j])
				{
				case 1:
					g.setColor(Color.RED);
					g.fillRect(40*j,40*i,40,40);
					break;
				case 2:
					g.setColor(Color.BLUE);
					g.fillRect(40*j, 40*i, 40, 40);
					break;
				case 3:
					g.setColor(Color.CYAN);
					g.fillOval(40*j, 40*i, 40, 40);
					break;
				case 4:
					g.setColor(Color.YELLOW);
					g.fillOval(40*j, 40*i, 40, 40);
					break;
				
					
				}
			}
		}
		
		
		
	}
	@Override
	public void keyPressed(KeyEvent arg0) {
		switch(arg0.getKeyCode())
	{
		case 37:
			obliczenia.ruszaj('l');
			break;
		case 38:
			obliczenia.ruszaj('g');
			break;
		case 39:
			obliczenia.ruszaj('p');
			break;
		case 40:
			obliczenia.ruszaj('d');
			break;
		
			
	
		}
		
	}
	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}

class Zadanie extends TimerTask{

	   
    int plansza[][] = new int[10][14];
    int poz1 = 4;
    int poz2 = 7;
    public void run() {
    	one_class.apki.repaint();
    	
    }
    public void zrobPlansze() 
    {
    	int plansza2[][] = {
    			{1,1,1,1,1,1,1,1,1,1,1,1,0,0},
    			{1,0,0,0,0,1,0,0,0,0,0,1,1,1},
    			{1,0,0,0,0,1,0,3,0,0,3,0,0,1},
    			{1,0,0,0,0,1,3,1,1,1,1,0,0,1},
    			{1,0,0,0,0,3,0,4,0,1,1,0,0,1},
    			{1,0,0,0,0,1,0,1,0,0,3,0,1,1},
    			{1,1,1,1,1,1,0,1,1,1,0,3,0,1},
    			{0,0,1,0,3,0,0,1,0,3,0,3,0,1},
    			{0,0,1,0,0,0,0,1,0,0,0,0,0,1},
    			{0,0,1,1,1,1,1,1,1,1,1,1,1,1},
    			{0,0,1,1,1,1,1,1,1,1,1,1,1,1}
    			
    	};
    	for(int i=0; i<plansza.length;i++)
    	{
    		for(int j = 0; j < plansza[0].length;j++)
    		{
    			plansza[i][j] = plansza2[i][j];
    		}
    	}
    }
	public void ruszaj(char c) {
		
		switch(c)
		{
		case 'l':
			if(plansza[poz1][poz2-1]==0)
			{
				plansza[poz1][poz2-1]=4;
				plansza[poz1][poz2] = 0;
				poz2--;
			}else if((plansza[poz1][poz2-1] ==3) && (plansza[poz1][poz2-2] == 0))
			{
				plansza[poz1][poz2-2] =3;
				plansza[poz1][poz2-1] = 4;
				plansza[poz1][poz2] = 0;
				poz2--;
			}
				
			
			break;
		case 'p':
			if(plansza[poz1][poz2+1]==0)
			{
				plansza[poz1][poz2+1]=4;
				plansza[poz1][poz2] = 0;
				poz2++;
			}else if((plansza[poz1][poz2+1] ==3) && (plansza[poz1][poz2+2] == 0))
			{
				plansza[poz1][poz2+2] =3;
				plansza[poz1][poz2+1] = 4;
				plansza[poz1][poz2] = 0;
				poz2++;
			}
		
			break;
		case 'g':
			if(plansza[poz1-1][poz2]==0)
			{
				plansza[poz1-1][poz2]=4;
				plansza[poz1][poz2] = 0;
				poz1--;
			}else if((plansza[poz1-1][poz2] ==3) && (plansza[poz1-2][poz2] == 0))
			{
				plansza[poz1-2][poz2] =3;
				plansza[poz1-1][poz2] = 4;
				plansza[poz1][poz2] = 0;
				poz1--;
			}
			break;
		case 'd':
			if(plansza[poz1+1][poz2]==0)
			{
				plansza[poz1+1][poz2]=4;
				plansza[poz1][poz2] = 0;
				poz1++;
			}else if((plansza[poz1+1][poz2] ==3) && (plansza[poz1+2][poz2] == 0))
			{
				plansza[poz1+2][poz2] =3;
				plansza[poz1+1][poz2] = 4;
				plansza[poz1][poz2] = 0;
				poz1++;
			}
			break;
		
		
	}
	}
 
    
    

		
		
		
		
//one_class.apki.repaint();
		
 
//		
//		switch(kierunek)
//		{
//		case 1:
//			licznik++;
//			poz1++;
//			if(licznik == 200)
//			{
//				licznik = 0;
//				kierunek = 2;
//				
//			}
//			break;
//        case 2:
//        	licznik++;
//        	poz2++;
//        	if(licznik == 200)
//        	{
//        		licznik = 0;
//        		kierunek= 3;
//        	}
//			break;
//        case 3:
//        	licznik++;
//        	poz1--;
//        	if(licznik == 200)
//        	{
//        		licznik = 0;
//        		kierunek = 4;
//        	}
//        	
//        	break;
//        case 4:
//        	licznik++;
//        	poz2--;
//        	if(licznik == 200)
//        	{
//        		licznik = 0;
//        		kierunek = 1;
//        	}
//        	break;
//		
//		}
    	
	

}

