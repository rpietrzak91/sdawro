package append_users;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;

public class dialog extends JFrame implements ActionListener{
	
	JTextArea notatnik;
	JScrollPane scroll;
	JLabel listausers;
	JButton bdodajuzytkownika, zapiszuzytkownika, usunuzytkownika;
	JTable tabela;
	private dodawanieUzytkownika dodawanieUzytkownika;

	
	
	
	public dialog() // tworzymy konstruktor dla naszej klasy dialog
	{
		
		setSize(600,350);
		setTitle("Użytkownicy");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(null);
		
		listausers = new JLabel();
		listausers.setText("Lista użytkowników: ");
		listausers.setBounds(20, 15, 150, 20);
		add(listausers);
		
		
		
		notatnik = new JTextArea();
		scroll = new JScrollPane(notatnik);
		scroll.setBounds(20, 40, 380, 250);
		add(scroll);
		
		
		bdodajuzytkownika = new JButton("Dodaj użytkownika");
		bdodajuzytkownika.setBounds(400, 90, 150, 20);
		add(bdodajuzytkownika);
		bdodajuzytkownika.addActionListener(this);
		
		zapiszuzytkownika = new JButton("Zapisz do pliku");
		zapiszuzytkownika.setBounds(400, 120, 150, 20);
		add(zapiszuzytkownika);
		zapiszuzytkownika.addActionListener(this);
		
		
		usunuzytkownika = new JButton("Usuń użytkownika");
		usunuzytkownika.setBounds(400, 150, 150, 20);
		add(usunuzytkownika);
		usunuzytkownika .addActionListener(this);

	}


	public static void main(String[] args) {
		
		dialog obiektdialog = new dialog();
		obiektdialog.setVisible(true);



	}


	public void actionPerformed(ActionEvent e) {
	
		Object dg = e.getSource();
		
		if(dg == bdodajuzytkownika )
		{
		if(dodawanieUzytkownika == null)
		{
			dodawanieUzytkownika = new dodawanieUzytkownika(this);
			dodawanieUzytkownika.setVisible(true);
			
			
			if(dodawanieUzytkownika.isOK())
			{
				
				notatnik.append(dodawanieUzytkownika.getLogin() + " | ");
				notatnik.append(dodawanieUzytkownika.getPassword() + "\n");
				
				
			}
		}
		else
		{
			dodawanieUzytkownika = new dodawanieUzytkownika(this);
			dodawanieUzytkownika.setVisible(true);
			notatnik.append(dodawanieUzytkownika.getLogin() + " | ");
			notatnik.append(dodawanieUzytkownika.getPassword() + "\n");
		}
		}
		
		if(dg == zapiszuzytkownika)
		{
			JFileChooser fc = new JFileChooser();
			if(fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
			{
				File plik = fc.getSelectedFile();
				try {
					PrintWriter pw = new PrintWriter(plik);
					Scanner scaner = new Scanner(notatnik.getText());
					while(scaner.hasNext())
					{
						pw.println(scaner.nextLine() + "\n");
						
					}
					pw.close();
					
				} catch (FileNotFoundException e1) {
					
					e1.printStackTrace();
				}
				
			}
		}

	}




}

class dodawanieUzytkownika extends JDialog implements ActionListener
{

	JLabel rejestracja, llogin, lhaslo, lwiek;
	JTextField blogin, bwiek;
	JPasswordField haslo;
	JButton rejestruj_uztkownika, cancel;
	boolean okData;
	
	
	
	public dodawanieUzytkownika(JFrame owner)
	{
		super(owner, "Dodawnie użytkowników", true);
		setSize(300,300);
		setLayout(null);
		
		rejestracja = new JLabel("Rejestracja:");
		rejestracja.setBounds(100, 10, 100, 20);
		add(rejestracja);
		
		llogin = new JLabel("Login:");
		llogin.setBounds(40, 50, 100, 20);
		add(llogin);
		
		blogin = new JTextField("");
		blogin.setBounds(90,50,150,20);
		add(blogin);
		
		lhaslo = new JLabel("Hasło:");
		lhaslo.setBounds(40, 100, 100, 20);
		add(lhaslo);
		
		haslo = new JPasswordField("");
		haslo.setBounds(90,100,150,20);
		add(haslo);
		
		rejestruj_uztkownika = new JButton("Rejestracja");
		rejestruj_uztkownika.setBounds(30,150,100,30);
		add(rejestruj_uztkownika);	
		rejestruj_uztkownika.addActionListener(this);
		
		cancel = new JButton("Anuluj");
		cancel.setBounds(160,150,100,30);
		add(cancel);	
	
	}
	
	 String getLogin()
	{
		return blogin.getText();
	}
	
	 String getPassword()
	{
		return haslo.getText();
	}
	
	boolean isOK()
	{
		return okData;
	}
	
	
	
	
	public void actionPerformed(ActionEvent e) {
		
		Object spr = e.getSource();
		if(spr == rejestruj_uztkownika)
		{
			okData = true;
			setVisible(false);
		}
		else if(spr == cancel)
		{
			okData = false;
			setVisible(false);
		}
		
		
		
	}
	

}
