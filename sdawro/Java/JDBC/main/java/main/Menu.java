package main;


import dao.KsiazkaDao;
import dao.UzytkownikDao;
import dao.impl.KsiazkaDaoImpl;
import dao.impl.UzytkownikDaoImpl;
import de.vandermeer.asciitable.v2.RenderedTable;
import de.vandermeer.asciitable.v2.V2_AsciiTable;
import de.vandermeer.asciitable.v2.render.V2_AsciiTableRenderer;
import de.vandermeer.asciitable.v2.render.WidthAbsoluteEven;
import model.Ksiazka;
import model.Uzytkownik;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class Menu {

    private KsiazkaDao ksiazkaDao;
    private UzytkownikDao uzytkownikDao;

    public Menu() {
        this.ksiazkaDao = new KsiazkaDaoImpl();
        this.uzytkownikDao = new UzytkownikDaoImpl();
    }

    public void wyswietlProgram() {
        Scanner scanner = new Scanner(System.in);
        String wybor;
        System.out.println("1 - dodaj uzytkownika");
        System.out.println("2 - dodaj ksiazke");

        System.out.println("3 - usun uzytkownika");
        System.out.println("4 - usun ksiazke");

        //System.out.println("5 - wypozycz");
       // System.out.println("6 - zwroc");

        System.out.println("7 - wyswietl biblioteke");

        System.out.println("q - zakoncz program");

        do {
            System.out.print("Twoj wybor: ");
            wybor = scanner.nextLine();
            switch (wybor) {
                case "1": {
                	
                	System.out.println("Podaj imie: ");
                	String imie = scanner.nextLine();
                	System.out.println("Podaj nazwisko: ");
                	String nazwisko = scanner.nextLine();
                	
                	Uzytkownik uzytkownik = new Uzytkownik(imie,nazwisko);
                	try {
                		uzytkownikDao.zapisz(uzytkownik);
                	}catch(SQLException e)
                	{
                		System.err.println("Wyst�pi� b��d: " + e.getMessage());
                	}
                }

                break;
                case "2": {
                	System.out.println("Podaj tytul: ");
                	String tytul = scanner.nextLine();
                	System.out.println("Podaj autora: ");
                	String autor = scanner.nextLine();
                	
                	Ksiazka ksiazka = new Ksiazka(tytul,autor);
                	
                	try {
						ksiazkaDao.zapisz(ksiazka);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

                }
                break;
                case "3": {
                	
                	List<Uzytkownik> uzytkowniklista;
					try {
						uzytkowniklista = uzytkownikDao.znajdzWszystkich();
						wypiszUzytkownikow(uzytkowniklista);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                	
					System.out.println("Kt�rego u�ytkownika chcesz usun�� ?");
					int usunuzytkownika = scanner.nextInt();
					try {
						uzytkownikDao.usun(usunuzytkownika);
						
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                
					
                	
                	
                	

                }
                break;
                case "4": {
                	
                	

                }
                break;
                case "5": {


                }
                break;
                case "6": {

                }
                break;
                case "7": {
                	
                	List<Uzytkownik> uzytkowniklista;
					try {
						uzytkowniklista = uzytkownikDao.znajdzWszystkich();
						wypiszUzytkownikow(uzytkowniklista);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                	
                	

                }
                break;
                case "q":
                    System.out.println("Koniec programu");
                    break;
            }
        } while (!wybor.equals("q"));
    }

    private void wypiszUzytkownikow(List<Uzytkownik> uzytkownicy) {
        V2_AsciiTable table = new V2_AsciiTable();
        table.addRule();
        table.addRow("id", "imie", "nazwisko");
        table.addStrongRule();
        for (Uzytkownik uzytkownik : uzytkownicy) {
            table.addRow(uzytkownik.getId(), uzytkownik.getImie(), uzytkownik.getNazwisko());
            table.addRule();
        }
        V2_AsciiTableRenderer renderer = new V2_AsciiTableRenderer();
        renderer.setWidth(new WidthAbsoluteEven(76));

        RenderedTable renderedTable = renderer.render(table);
        System.out.println("\n\n Lista uzytkownikow: ");
        System.out.println(renderedTable);
    }

    private void wypiszKsiazki(List<Ksiazka> ksiazki) {

    }

}
