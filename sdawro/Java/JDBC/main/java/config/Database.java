package config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

	private static final String USERNAME = "root";
	private static final String PASSWORD = "root";
	private static final String URL = "jdbc:mysql://localhost:3306/sda_jdbc";
	private Connection connection;

	public Database() {
		try {
			Class.forName("com.mysql.jdbc.Driver"); // sterownik do bazy danych !
		} catch (ClassNotFoundException e) {
			System.out.println("Nie mozna zaladowac sterownika");
			System.exit(0);
		}
	}

	public Connection otworzPolaczenie() throws SQLException {

		if (connection == null) {
			connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
		}

		return connection;
	}

	public void zamknijPolaczenie() throws SQLException {
		if (connection != null) {
			connection.close();
			connection = null;
		}
	}

}
