package dao.impl;

import dao.UzytkownikDao;
import model.Uzytkownik;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;

import config.Database;

public class UzytkownikDaoImpl implements UzytkownikDao {
	private Database database = new Database();

    @Override
    public void zapisz(Uzytkownik uzytkownik) throws SQLException {
    	// pobierz polaczenie
    	Connection connection = database.otworzPolaczenie();
    	
    	// przygotuj obiekt typu PreparedStatement
    	String sql = "INSERT INTO uzytkownik(imie,nazwisko) VALUES(?,?)";
    	PreparedStatement statement = connection.prepareStatement(sql);
    	
    	// ustaw parametry zapytania
    	statement.setString(1, uzytkownik.getImie());
    	statement.setString(2, uzytkownik.getNazwisko());
    	
    	//	wykonaj zapytanie
    	statement.executeUpdate();
    	// zamknij połączenie
    	database.zamknijPolaczenie();
    }

    @Override
    public Uzytkownik znajdz(int id) throws SQLException {
    	Uzytkownik uzytkownik = null;
    	// pobierz polaczenie
    	Connection connection = database.otworzPolaczenie();
    	
    	// przygotuj obiekt typu PreparedStatement
    	String sql = "SELECT id, imie, nazwisko FROM uzytkownik WHERE id=?";
    	PreparedStatement statement = connection.prepareStatement(sql);
    	// ustaw parametry zapytania
    	statement.setInt(1, id);
    	// wykonaj zapytanie -> zawroc ResultSet
    	ResultSet result = statement.executeQuery();
    	//pobrac dane z ResultSet i przetotrzymy je z obiekty javome
    	while(result.next()) { // przeskakujemy po kolejnych rekordach tabeli
    		String imie = result.getString("imie");//nazwa kolumny z bazy danych
    		String nazwisko = result.getString("nazwisko"); // nazwisko kolumny z bazy danych
    		uzytkownik = new Uzytkownik(id,imie,nazwisko);
    	}
    	database.zamknijPolaczenie();
        return uzytkownik;
    }

    @Override
    public List<Uzytkownik> znajdzWszystkich() throws SQLException {
    	Uzytkownik uzytkownik = null;
    	ArrayList<Uzytkownik> uzytkowniklista = new ArrayList<Uzytkownik>();
    	// pobierz polaczenie
    	Connection connection = database.otworzPolaczenie();
    	// przygotuj obiekt typu PreparedStatement
    	String sql = "SELECT id, imie, nazwisko FROM uzytkownik";
    	PreparedStatement statement = connection.prepareStatement(sql);
    	ResultSet result = statement.executeQuery();
    	while(result.next()) { // przeskakujemy po kolejnych rekordach tabeli
    		int id = result.getInt("id");
    		String imie = result.getString("imie");//nazwa kolumny z bazy danych
    		String nazwisko = result.getString("nazwisko"); // nazwisko kolumny z bazy danych
    		uzytkownik = new Uzytkownik(id,imie,nazwisko);
    		uzytkowniklista.add(uzytkownik);
    		
    	}
        return uzytkowniklista;
    }

    @Override
    public void usun(int id) throws SQLException {
    	Uzytkownik uzytkownik = null;
    	// pobierz polaczenie
    	Connection connection = database.otworzPolaczenie();
    	
    	// przygotuj obiekt typu PreparedStatement
    	String sql = "DELETE  FROM uzytkownik WHERE id=?";
    	PreparedStatement statement = connection.prepareStatement(sql);
    	// ustaw parametry zapytania
    	statement.setInt(1, id);
    	
//    	wykonaj zapytanie
    	statement.executeUpdate();
    }
}














