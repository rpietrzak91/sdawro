package dao.impl;

import dao.KsiazkaDao;
import model.Ksiazka;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import config.Database;

public class KsiazkaDaoImpl implements KsiazkaDao {
	private Database database = new Database();
   
    public void zapisz(Ksiazka ksiazka) throws SQLException {
    	// pobierz polaczenie
    	Connection connection = database.otworzPolaczenie();
    	
    	// przygotuj obiekt typu PreparedStatement
    	String sql = "INSERT INTO ksiazka(tytul,autor) VALUES(?,?)";
    	PreparedStatement statement = connection.prepareStatement(sql);
    	
    	// ustaw parametry zapytania
    	statement.setString(1, ksiazka.getTytul());
    	statement.setString(2, ksiazka.getAutor());
    	
    	//wykonaj zapytanie
    	statement.executeUpdate();
    	// zamknij połączenie
    	database.zamknijPolaczenie();
    	

    }

    @Override
    public Ksiazka znajdz(int id) throws SQLException {
        return null;
    }

    @Override
    public List<Ksiazka> znajdzWszystkie() throws SQLException {
        return null;
    }

	@Override
	public void usun(int id) throws SQLException {
		
		
	}
}