package celsjusza_na_fahrenheita;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class celsjusza_na_fahrenheita extends JFrame implements ActionListener {
	
	private JLabel jfahrenheita,jcelsjusza ;
	private JTextField txcelsjusza,txfahrenheita;
	private JButton jprzelicz;
	private JCheckBox chWielkie;
	private double tempCelsius, tempFahrenheit;
	private ButtonGroup grradio;
	private JRadioButton red,green,blue;
	private JRadioButton rbCtoF, rbFtoC;
	private ButtonGroup radioPanel;
	
	celsjusza_na_fahrenheita()
	{
		setSize(480,380);
		setTitle("Zamiana Celsjusza na Fahrenheita");
		setLayout(null);
		
		jcelsjusza = new JLabel("Podaj stopnie w celsjusza:");
		jcelsjusza.setBounds(50,40,150,50);
		add(jcelsjusza);
		
		jfahrenheita = new JLabel("Podaj stopnie w fahrenheita:");
		jfahrenheita.setBounds(40,120,200,50);
		add(jfahrenheita);
		
		txcelsjusza = new JTextField("");
		txcelsjusza.setBounds(206,47,170,35);
		add(txcelsjusza);
		txcelsjusza.addActionListener(this);
		txcelsjusza.setToolTipText("Podaj temperatur� w stopniach Celsiusza");
		
		txfahrenheita = new JTextField("");
		txfahrenheita.setBounds(205,125,170,35);
		add(txfahrenheita);
		txfahrenheita.setForeground(Color.BLACK);
		txfahrenheita.addActionListener(this);
		
		jprzelicz = new JButton("Przelicz");
		jprzelicz.setBounds(100,200,170,35);
		add(jprzelicz);
		jprzelicz.addActionListener(this);
		
		chWielkie = new JCheckBox("Background Fahren");
		chWielkie.setToolTipText("Zmiana t�a po wci�ni�ciu przycisku Button");
		chWielkie.setBounds(295,205,150,20);
		add(chWielkie);
		
		
		red = new JRadioButton("Red", false);
		grradio = new ButtonGroup();
		red.setBounds(50,310,100,20);
		grradio.add(red);
		add(red);
		red.addActionListener(this);
		
		green = new JRadioButton("Green", false);
		green.setBounds(150,310,100,20);
		grradio.add(green);
		add(green);
		green.addActionListener(this);
		
		blue= new JRadioButton("Blue", false);
		blue.setBounds(250,310,100,20);
		grradio.add(blue);
		add(blue);
		blue.addActionListener(this);
		
		
		radioPanel = new ButtonGroup();
		rbCtoF = new JRadioButton("Celsius to Fahrenheit");
		rbCtoF.setBounds(50,260,160,20);
		rbCtoF.setSelected(true);
		radioPanel.add(rbCtoF);
		add(rbCtoF);
		
		rbFtoC = new JRadioButton("Fahrenheit to Celsius");
		rbFtoC.setBounds(210,260,160,20);
		radioPanel.add(rbFtoC);
		add(rbFtoC);
	}
	
	
	
	
	

	public static void main(String[] args) {
	
		celsjusza_na_fahrenheita okienko = new celsjusza_na_fahrenheita();
		okienko.setVisible(true);
		okienko.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


	}

	public void actionPerformed(ActionEvent e) {
		
		Object zrodlo = e.getSource();
		if(zrodlo == jprzelicz  )
		{
			if(rbCtoF.isSelected())
			{
				tempCelsius=Double.parseDouble(txcelsjusza.getText());
				tempFahrenheit = 32 +(9/5) * tempCelsius;
				txfahrenheita.setText(String.valueOf(tempFahrenheit));
			}
			else
			{
				tempFahrenheit = Double.parseDouble(txfahrenheita.getText());
				tempCelsius = (tempFahrenheit - 32.0) * (5.0/9.0);
				txcelsjusza.setText(String.valueOf(tempCelsius));
				
			}
			
			if(chWielkie.isSelected())
			{
				txfahrenheita.setBackground(Color.orange);//wartosc CheckBox sprawdzana jest po wci�ni�ciu JButton
			}
			else
			{
				txfahrenheita.setBackground(Color.WHITE);
				
			}
	
		}
		else if(zrodlo == red)
		{
			txfahrenheita.setFont(new Font("SansSerif", Font.BOLD, 15));
			txfahrenheita.setForeground(Color.RED);
		}
		else if(zrodlo == green)
		{
			txfahrenheita.setFont(new Font("SansSerif", Font.BOLD, 15));
			txfahrenheita.setForeground(Color.GREEN);
		}
		else if(zrodlo == blue)
		{
			txfahrenheita.setFont(new Font("SansSerif", Font.BOLD, 15));
			txfahrenheita.setForeground(Color.BLUE);
		}
		
		if(zrodlo == txcelsjusza)
		{
			tempCelsius=Double.parseDouble(txcelsjusza.getText());
			tempFahrenheit = 32 +(9/5) * tempCelsius;
			txfahrenheita.setText(String.valueOf(tempFahrenheit));
			
		}else if(zrodlo == txfahrenheita)
		{
			tempFahrenheit = Double.parseDouble(txfahrenheita.getText());
			tempCelsius = (tempFahrenheit - 32.0) * (5.0/9.0);
			txcelsjusza.setText(String.valueOf(tempCelsius));
		}
			
	}

}
