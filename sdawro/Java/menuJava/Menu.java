import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

public class Menu extends JFrame implements ActionListener {
	
	private JMenuBar menubar;
	JButton wybierzKolor, showpassword;
	JComboBox czarny,zielony,niebieskie,czerwony,pomaranczowy;
	JComboBox changecolor;
	private JMenu menuPlik,menuNarzedzia,menuKonfiguracja,pomocZdalna,menuOprogramie;
	private JMenuItem otworzPlik, zapiszPlik, wyjscie, narzedzia1, narzedzia2, konfipodst, konfizawanso, oProgramie, Polaczenie, pmKopiuj, pmWklej, pmDolacz, pmWytnij;
	private JTextArea notatnik, description;
	private JTextField wprowadz_dane,tSzukamy ;
	private JButton tSzukaj, bDodajUzytkownika;
	JPopupMenu popup;
	String kopiowanie;
	JPasswordField haslo;
	private uzytkownik uzytkownik;
	int i = 0;
	
	public Menu()
	{
		setSize(600,900);
		setTitle("Menu Java");
		setLayout(null);
		
		menubar = new JMenuBar(); // stworzenie nowego obiektu menubar
		setJMenuBar(menubar); // dodanie menuBar do naszej ramki 
		
		// Zak�adka Plik
		
		menuPlik = new JMenu("Plik");
		otworzPlik = new JMenuItem("Otw�rz");
		otworzPlik.addActionListener(this);
		otworzPlik.setAccelerator(KeyStroke.getKeyStroke("ctrl O"));
		zapiszPlik = new JMenuItem("Zapisz");
		zapiszPlik.addActionListener(this);
	    wyjscie = new JMenuItem("Wyjscie");
	    wyjscie.addActionListener(this);
	    wyjscie.setAccelerator(KeyStroke.getKeyStroke("ctrl X"));
		
		menuPlik.add(otworzPlik);
		menuPlik.add(zapiszPlik);
		menuPlik.addSeparator();
		menuPlik.add(wyjscie);
		
		
		// Koniec Zak�adka Plik
		
		// Zak�adka Narz�dzia
		
		menuNarzedzia = new JMenu("Narz�dzia");
		narzedzia1 = new JMenuItem("Narz�dzia 1");
		narzedzia1.addActionListener(this);
		narzedzia2 = new JMenuItem("Narz�dzia 2");
		menuNarzedzia.add(narzedzia1);
		menuNarzedzia.add(narzedzia2);
		
		// Koniec Zak�adka Plik
		
		
		// Zak�adka Konfiiguracja
		
		menuKonfiguracja = new JMenu("Konfiguracja");
		konfipodst = new JMenuItem("Konfiguracja Podstawowa");
		konfizawanso = new JMenuItem("Konfiguracja Zaawansowana");
		menuKonfiguracja.add(konfipodst);
		menuKonfiguracja.add(konfizawanso);
		
		// Koniec Zak�adka Plik
		
		
		// Zak�adka Pomoc Zdalna
		
		pomocZdalna = new JMenu("Pomoc Zdalna");
		Polaczenie = new JMenuItem("Po��czenie");
		pomocZdalna.add(Polaczenie);
		Polaczenie.addActionListener(this);
		
		// Koniec Zak�adka Plik
		
		
		// Zak�adka Pomoc Pomoc
		
		menuOprogramie = new JMenu("Pomoc");
		oProgramie = new JMenuItem("o Programie");
		menuOprogramie.add(oProgramie);
		
		// Koniec Zak�adka Plik
		
		menubar.add(menuPlik);
		menubar.add(menuNarzedzia);
		menubar.add(menuKonfiguracja);
		menubar.add(pomocZdalna);
		menubar.add(menuOprogramie);
		
		// Button
//	
//		selectfile = new JButton("Wybierz plik..");
//		selectfile.setBounds(100,100,150,60);
//		add(selectfile);
//		selectfile.addActionListener(this);
		
		notatnik = new JTextArea();
		JScrollPane scroll = new JScrollPane(notatnik);
		scroll.setBounds(40, 90, 300, 270);
		add(scroll);
		
//		wprowadz_dane = new JTextField();
//		wprowadz_dane.setBounds(40, 20, 280,20);
//		add(wprowadz_dane);
//		wprowadz_dane.addActionListener(this);
		String desc = "Opis Programu: \n 1.Odczyt plik�w oraz zapis plik�w w oknie TextArea. \n 2. Mo�liwo�� otwarcia okna skr�tami klawiatury np: ctrl O \n 3. Zmiana koloru za pomoc� JComboBox w wprowdzonym tekscie. \n 4.Zmiana koloru tekstu za pomoc� rowijanej palety kolor�w JColorChoose \n 5. Mo�liwo�� kopiowania,wklejani, do��czania tekstu realizowane przez JTextArea(Klikamy prawym i wybieram interesuj�c� nas opcj�)";
		description = new JTextArea(desc);
		JScrollPane desc1 = new JScrollPane(description);
		desc1.setBounds(40, 20, 440,60);
		add(desc1);
		
		
		
//		wykonaj = new JButton("Sprawdz");
//		wykonaj.setBounds(220, 20, 90,20);
//		add(wykonaj);
//		wykonaj.addActionListener(this);
		

		tSzukamy = new JTextField("");
		tSzukamy.setBounds(40, 400, 170, 20);
		tSzukamy.setText("Ci�g wyraz�w do przeszukania");
		add(tSzukamy);
		tSzukamy.addActionListener(this);
		
		tSzukaj = new JButton("Szukaj");
		tSzukaj.setBounds(210, 400, 100, 20);
		add(tSzukaj);
		tSzukaj.addActionListener(this);
		
		
		popup = new JPopupMenu();
		
		pmKopiuj = new JMenuItem("Kopiuj");
		pmKopiuj.addActionListener(this);
		pmKopiuj.setAccelerator(KeyStroke.getKeyStroke("ctrl Z"));
		
		
		
		pmWklej = new JMenuItem("Wklej");
		pmWklej.addActionListener(this);
		
		pmDolacz = new JMenuItem("Do��cz");
		pmDolacz.addActionListener(this);
		
		pmWytnij = new JMenuItem("Wytnij");
		pmWytnij.addActionListener(this);
		
		popup.add(pmKopiuj);
		popup.add(pmWklej);
		popup.add(pmDolacz);
		popup.add(pmWytnij);
		
		notatnik.setComponentPopupMenu(popup);
		
		changecolor = new JComboBox();
		changecolor.setBounds(350, 100, 130, 30);
		changecolor.addActionListener(this);
		
		bDodajUzytkownika = new JButton("Dodaj u�ytkownika");
		bDodajUzytkownika.setBounds(370, 150, 150, 30);
		bDodajUzytkownika.addActionListener(this);
		add(bDodajUzytkownika);
		

	
		changecolor.addItem("Czerwony");
		changecolor.addItem("Zielony");
		changecolor.addItem("Niebieski");
		changecolor.addItem("Czarny");
		changecolor.addItem("Pomara�czowy");
		add(changecolor);
		
		wybierzKolor = new JButton("Wybierz Kolor");
		wybierzKolor.setBounds(330, 400, 140, 20);
		wybierzKolor.addActionListener(this);
	    add(wybierzKolor);
	    
	    
	    haslo = new JPasswordField();
	    haslo.setBounds(100, 500, 100, 30);
	    add(haslo);
	    haslo.addActionListener(this);
	    
	    showpassword = new JButton("Poka� has�o");
	    showpassword.setBounds(210, 500, 150, 30);
	    add(showpassword);
	    showpassword.addActionListener(this);
	    

	}
	
	  
	




	public static void main(String[] args) {
		
		Menu windowMenu = new Menu();
		windowMenu.setVisible(true);
		windowMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	@Override
	public void actionPerformed(ActionEvent f) {
		
		
		
		
		Object wM = f.getSource();
		if(wM == bDodajUzytkownika)
		{
		if(uzytkownik == null)
		{
			uzytkownik pass = new uzytkownik(this);
			pass.setVisible(true);
			pass.setFocus();
			
			if(pass.isOK() == true)
			{
			notatnik.append(pass.getUser() + ",");
			notatnik.append(pass.getPassword() + "\n");
			
				
				
			}
			
			
			
			
		}
		}
		
		
		
		if(wM == wyjscie) 
		{
			int odp = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz wyj��", "Wyj�cie", JOptionPane.YES_NO_OPTION);
			if(odp == JOptionPane.YES_OPTION)
			dispose();
			else if (odp == JOptionPane.NO_OPTION);
			else if(odp == JOptionPane.CLOSED_OPTION)
			JOptionPane.showMessageDialog(null, "Cieszymy si�, �e zosta�e� z nami");
		}
		
		if(wM == narzedzia1)
		{
			String metry = JOptionPane.showInputDialog("Podaj d�ugo�� w metrach: ");
			double Smetry = Double.parseDouble(metry);
			double stopy = Smetry/0.3048;
			//String sStopy = String.format("%.2f", stopy);
			JOptionPane.showMessageDialog(null, Smetry + " metr�w " + " to " + stopy + "stop");

		}
		
		if(wM == otworzPlik)
		{
			JFileChooser fc = new JFileChooser();
			
			if(fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
			{
				File plik = fc.getSelectedFile();
				//JOptionPane.showMessageDialog(null, plik.getAbsolutePath());
				try {
					Scanner scaner = new Scanner(plik);
					while(scaner.hasNext())
						notatnik.append(scaner.nextLine() + "\n");
					
				} catch (FileNotFoundException e) {
					
					e.printStackTrace();
				}
			
				
			 
			}
		}
		
		if(wM == zapiszPlik)
		{
			JFileChooser fc = new JFileChooser();
			if(fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
			{
				File plik = fc.getSelectedFile();
				JOptionPane.showMessageDialog(null, "Zapisany plik to: " + plik);
				
				try {
					PrintWriter pw = new PrintWriter(plik);
					Scanner scaner = new Scanner(notatnik.getText());
					while(scaner.hasNext())
					pw.println(scaner.nextLine() + "\n");
					pw.close();
					
					
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
	
			
			}
		}
		
		if(wM == Polaczenie)
		{
			String zdalna = JOptionPane.showInputDialog("Wpisz adres zdalnego hosta: ");
			Integer Kzdalna = Integer.parseInt(zdalna);
			JOptionPane.showMessageDialog(null, "Host z, kt�rym pr�bujesz sie po��czy� to: " + Kzdalna);
		}
		
		
		// Wyszukiwanie JTextArea
		
		if(wM == tSzukaj)
		{
			String getNotatnik = notatnik.getText();
			String getSzukane = tSzukamy.getText();
			String wystapienia = " ";
			int iloscPowtorzen = 0;
			int aktualnaPozycja = 0;
			int i = 0;
			
			while((iloscPowtorzen = getNotatnik.indexOf(getSzukane, aktualnaPozycja)) != -1)
			{
				aktualnaPozycja = iloscPowtorzen + getSzukane.length();
				wystapienia = wystapienia +  " " +  iloscPowtorzen;
				i++;
		
			}
			JOptionPane.showMessageDialog(null, "Ilo�� powt�rze� slowa: " + getSzukane + " = " + i + "Pozycje powt�rzen: " + wystapienia);
			
			
		}
		
		if(wM == pmKopiuj)
		{
			kopiowanie = notatnik.getSelectedText();
			System.out.println("ABC");
			
		}
		
		if(wM == pmWklej)
		{
			notatnik.append(kopiowanie + "\n");
		}
		
		if(wM == pmDolacz)
		{
			notatnik.insert(kopiowanie, notatnik.getCaretPosition());
		}
		
		if(wM == pmWytnij)
		{
			
			kopiowanie = notatnik.getSelectedText();
			int dlugosc = kopiowanie.length();
			kopiowanie.substring(3, dlugosc);
		
		}
		
		if(wM == changecolor)
		{
			String zmienKolor = changecolor.getSelectedItem().toString();
			
			if(zmienKolor.equals("Zielony"))
			{
				notatnik.setForeground(Color.green);
			}
			if(zmienKolor.equals("Czerwony"))
			{
				notatnik.setForeground(Color.RED);
			}
			if(zmienKolor.equals("Niebieski"))
			{
				notatnik.setForeground(Color.BLUE);
			}

		}	
		
		if(wM == wybierzKolor)
		{
			Color colortext =JColorChooser.showDialog(null, "Wybierz Kolor", Color.BLUE);
			notatnik.setForeground(colortext);
//			Color colorbg =JColorChooser.showDialog(null, "Wybierz Kolor", Color.BLUE);
//			notatnik.setBackground(colorbg);
		
			
		}
		
		if(wM == showpassword)	
		{
			String password = haslo.getText();
			JOptionPane.showMessageDialog(null, password);
		}
		
	
		
		}

	}



 class uzytkownik extends JDialog implements ActionListener {
     
	/**
	 * 
	 */

	private JLabel jlogin, jhaslo;
	private JTextField tlogin;
	private JPasswordField thaslo;
	private JButton blogin, bhaslo;
	private boolean okData;

	public uzytkownik(JFrame owner)
	{
		super(owner, "Wprowadz has�o", true);
		setSize(400,300);
		setLayout(null);
		
		jlogin = new JLabel("Login:");
		jlogin.setBounds(50, 10, 50, 20);
		add(jlogin);
		
		tlogin = new JTextField("");
		tlogin.setBounds(100, 10, 150, 20);
		add(tlogin);
		
		jhaslo = new JLabel("Has�o:");
		jhaslo.setBounds(50,50, 50, 20);
		add(jhaslo);
		
		thaslo = new JPasswordField("");
		thaslo.setBounds(100, 50, 150, 20);
		add(thaslo);
		thaslo.addActionListener(this);
		
		blogin = new JButton("Potwierdz");
		blogin.setBounds(30, 150, 150, 20);
		add(blogin);
		blogin.addActionListener(this);
		
		bhaslo = new JButton("Anuluj");
		bhaslo.setBounds(220, 150, 150, 20);
		add(bhaslo);
		bhaslo.addActionListener(this);
		
		
		
		
		
		
		
	}
	
	public String getUser()
	{
		return tlogin.getText();
		
	}
	
	public String getPassword()
	{
		return thaslo.getText();
		//return new String(jhaslo.getPassword());
		
	}
	
	public boolean isOK()
	{
		
		
		return okData;
	}
	
	public void setFocus()
	{
		jlogin.requestFocusInWindow();
	}
	
	
	
	
	public void actionPerformed(ActionEvent t) {
		
		Object g = t.getSource();
		
		if(g == blogin)
		{
			 okData = true;
			 setVisible(false);
		}
		else
		{
			okData = false;
		}
		
	}

}

				
	
		
