import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

public class MenuJava extends JFrame implements ActionListener {
	
	private JMenuBar menubar;
	private JMenu menuPlik,menuNarzedzia,menuKonfiguracja,pomocZdalna,menuOprogramie;
	private JMenuItem otworzPlik, zapiszPlik, wyjscie, narzedzia1, narzedzia2, konfipodst, konfizawanso, oProgramie, Polaczenie;
	
	
	public MenuJava()
	{
		setSize(400,400);
		setTitle("Menu Java");
		setLayout(null);
		
		menubar = new JMenuBar(); // stworzenie nowego obiektu menubar
		setJMenuBar(menubar); // dodanie menuBar do naszej ramki 
		
		// Zak�adka Plik
		
		menuPlik = new JMenu("Plik");
		otworzPlik = new JMenuItem("Otw�rz");
		otworzPlik.addActionListener(this);
		zapiszPlik = new JMenuItem("Zapisz");
		zapiszPlik.addActionListener(this);
	    wyjscie = new JMenuItem("Wyjscie");
	    wyjscie.addActionListener(this);
	    wyjscie.setAccelerator(KeyStroke.getKeyStroke("ctrl X"));
		
		menuPlik.add(otworzPlik);
		menuPlik.add(zapiszPlik);
		menuPlik.addSeparator();
		menuPlik.add(wyjscie);
		
		
		// Koniec Zak�adka Plik
		
		// Zak�adka Narz�dzia
		
		menuNarzedzia = new JMenu("Narz�dzia");
		narzedzia1 = new JMenuItem("Narz�dzia 1");
		narzedzia1.addActionListener(this);
		narzedzia2 = new JMenuItem("Narz�dzia 2");
		menuNarzedzia.add(narzedzia1);
		menuNarzedzia.add(narzedzia2);
		
		// Koniec Zak�adka Plik
		
		
		// Zak�adka Konfiiguracja
		
		menuKonfiguracja = new JMenu("Konfiguracja");
		konfipodst = new JMenuItem("Konfiguracja Podstawowa");
		konfizawanso = new JMenuItem("Konfiguracja Zaawansowana");
		menuKonfiguracja.add(konfipodst);
		menuKonfiguracja.add(konfizawanso);
		
		// Koniec Zak�adka Plik
		
		
		// Zak�adka Pomoc Zdalna
		
		pomocZdalna = new JMenu("Pomoc Zdalna");
		Polaczenie = new JMenuItem("Po��czenie");
		pomocZdalna.add(Polaczenie);
		Polaczenie.addActionListener(this);
		
		// Koniec Zak�adka Plik
		
		
		// Zak�adka Pomoc Pomoc
		
		menuOprogramie = new JMenu("Pomoc");
		oProgramie = new JMenuItem("o Programie");
		menuOprogramie.add(oProgramie);
		
		// Koniec Zak�adka Plik
		
		menubar.add(menuPlik);
		menubar.add(menuNarzedzia);
		menubar.add(menuKonfiguracja);
		menubar.add(pomocZdalna);
		menubar.add(menuOprogramie);
		
		// Button
//	
//		selectfile = new JButton("Wybierz plik..");
//		selectfile.setBounds(100,100,150,60);
//		add(selectfile);
//		selectfile.addActionListener(this);
		
		

	}
	
	  
	




	public static void main(String[] args) {
		
		MenuJava windowMenu = new MenuJava();
		windowMenu.setVisible(true);
		windowMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	@Override
	public void actionPerformed(ActionEvent f) {
		
		Object wM = f.getSource();
		
		if(wM == wyjscie) 
		{
			int odp = JOptionPane.showConfirmDialog(null, "Czy na pewno chcesz wyj��", "Wyj�cie", JOptionPane.YES_NO_OPTION);
			if(odp == JOptionPane.YES_OPTION)
			dispose();
			else if (odp == JOptionPane.NO_OPTION);
			else if(odp == JOptionPane.CLOSED_OPTION)
			JOptionPane.showMessageDialog(null, "Cieszymy si�, �e zosta�e� z nami");
		}
		
		if(wM == narzedzia1)
		{
			String metry = JOptionPane.showInputDialog("Podaj d�ugo�� w metrach: ");
			double Smetry = Double.parseDouble(metry);
			double stopy = Smetry/0.3048;
			//String sStopy = String.format("%.2f", stopy);
			JOptionPane.showMessageDialog(null, Smetry + " metr�w " + " to " + stopy + "stop");

		}
		
		if(wM == otworzPlik)
		{
			JFileChooser fc = new JFileChooser();
			if(fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
			{
				File plik = fc.getSelectedFile();
				JOptionPane.showMessageDialog(null, plik.getAbsolutePath());
			}
		}
		
		if(wM == zapiszPlik)
		{
			JFileChooser fc = new JFileChooser();
			if(fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION)
			{
				File plik = fc.getSelectedFile();
				JOptionPane.showMessageDialog(null, "Zapisany plik to: " + plik);
				
			}
		}
		
		if(wM == Polaczenie)
		{
			String zdalna = JOptionPane.showInputDialog("Wpisz adres zdalnego hosta: ");
			Integer Kzdalna = Integer.parseInt(zdalna);
			JOptionPane.showMessageDialog(null, "Host z, kt�rym pr�bujesz sie po��czy� to: " + Kzdalna);
		}
	
	}
		
	}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
