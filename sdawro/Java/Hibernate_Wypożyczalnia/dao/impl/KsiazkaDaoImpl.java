package dao.impl;

import config.Database;
import config.HibernateConfig;
import dao.KsiazkaDao;
import model.Ksiazka;
import model.Uzytkownik;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

public class KsiazkaDaoImpl implements KsiazkaDao {
    

    @Override
    public void zapisz(Ksiazka ksiazka) {
      
    	Session session = HibernateConfig.getSession();
    	session.save(ksiazka);
    	session.close();
    }

    @Override
    public Ksiazka znajdz(int id) {
        Ksiazka ksiazka = null;
        Session session = HibernateConfig.getSession();
        
        ksiazka = session.get(Ksiazka.class, id);
    	session.close();
    	return ksiazka;
    }

    @Override
    public List<Ksiazka> znajdzWszystkie() {
     
    	Session session = HibernateConfig.getSession();
    	Query query = session.createQuery("from Uzytkownik");
    	@SuppressWarnings("unchecked")
		List<Ksiazka> ksiazka = query.list();
    	
    	session.close();
    	return ksiazka;
    }

    @Override
    public void usun(int id) {
       
    	Session session = HibernateConfig.getSession();
    	session.beginTransaction();
    	Ksiazka ksiazka= znajdz(id);
    	session.delete(ksiazka);
    	
    	session.getTransaction().commit();
    	session.close();
    	
    	
    	
    }
}
