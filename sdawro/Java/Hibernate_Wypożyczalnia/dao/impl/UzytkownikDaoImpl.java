package dao.impl;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import config.Database;
import config.HibernateConfig;
import dao.UzytkownikDao;
import model.Uzytkownik;

public class UzytkownikDaoImpl implements UzytkownikDao {



    public void zapisz(Uzytkownik uzytkownik)  {
    	
    	Session session = HibernateConfig.getSession();
    	
    	session.save(uzytkownik);
    	session.close();

    }

    public Uzytkownik znajdz(int id) {
    	Uzytkownik uzytkownik = null;
    	
    	Session session = HibernateConfig.getSession();
    	uzytkownik = session.get(Uzytkownik.class, id);
    	session.close();
    	return uzytkownik;
    	

    }

    public List<Uzytkownik> znajdzWszystkich() {
       
    	Session session = HibernateConfig.getSession();
    	Query query = session.createQuery("from Uzytkownik");
    	@SuppressWarnings("unchecked")
		List<Uzytkownik> uzytkownicy = query.list();
    	
    	session.close();
    	return uzytkownicy;
    	
    	
    }

    public void usun(int id) throws SQLException {
    	Session session = HibernateConfig.getSession();
    	session.beginTransaction();
    	Uzytkownik uzytkownik = znajdz(id);
    	session.delete(uzytkownik);
    	
    	session.getTransaction().commit();
    	session.close();
    	
     
    }
}













