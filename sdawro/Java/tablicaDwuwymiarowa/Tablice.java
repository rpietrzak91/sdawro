
public class Tablice {

	public static void main(String[] args) {
		
		
		/**
		 * Tablica jednowymiarowa - lista zmiennych tego samego typu
		 * 
		 * Deklaracja zmiennej tablicowej
		 * 
		 * typ nazwa_zmiennej[]
		 */
		
	int tablicaJednowymiarowa[];
		
		/**
		 * 
		 * Alokacja pami�ci dlatablicy
		 * 
		 * nazwa+zmiennej = new typ[rozmiar]
		 */
		
//	 tablicaJednowymiarowa = new int[10];
//	 
//	 tablicaJednowymiarowa[1] = 10;
//	 System.out.println(tablicaJednowymiarowa[1]);
	 
//	 int tablica[] = {56,6546,46,67,87,98,96,89};
//	 
//	 for(int i = 0; i < tablica.length;i++)
//	 {
//		 System.out.println(tablica[i]);
//	 }
	 
	 // tablice dwuwymiarowe
	 
	 /**
	  * Tablice dwuwymiarowe - tablice tablic
	  * 
	  * typ nazwa_zmiennej[][]= new typ[rozmiar][rozmiar]
	  */
	 
      int tablicaDwuwymiarowa[][] = new int[6][9]; // nasza tablica b�dzie mia�a 6 wierszy na 9 kolumn
      int i,j,k = 0;
      
      for(i = 0; i < 6; i++)
      {
    	  for( j = 0; j < 9; j++)
    	  {
    		  tablicaDwuwymiarowa[i][j] = k;
    		  k++;
    		  
    	  }
      }
      for(i = 0; i < 6; i++)
      {
    	  for( j = 0; j < 9; j++)
    	  {
    		System.out.print(tablicaDwuwymiarowa[i][j] + " ");
    		  
    	  }
    	  System.out.println();
      }
	}
    
}
