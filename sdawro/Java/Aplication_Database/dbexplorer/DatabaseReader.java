package dbexplorer;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class DatabaseReader extends JPanel implements ListSelectionListener{
	ExplorerPanel explorerPanel;
	DataViewer dataViewer;
	public DatabaseReader() {
		
		super(new GridBagLayout());
		explorerPanel = new ExplorerPanel();
		dataViewer = new DataViewer();
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0; c.gridy = 0;c.anchor=GridBagConstraints.FIRST_LINE_START;
		c.weighty =1; c.fill = GridBagConstraints.VERTICAL;
		add(explorerPanel, c);
		c.gridx=1;c.weightx=1;c.weighty=1;c.fill = GridBagConstraints.BOTH;
		add(dataViewer,c);
		
		// dobieramy sie do listy tabel z poziomu database reader
		explorerPanel.tableList.addListSelectionListener(this);
		
		
		
	}
	public static void main(String[] args) {
		
		DatabaseReader dr = new DatabaseReader();
		JFrame frame = new JFrame("Database Reader");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(500, 500));
		frame.setContentPane(dr);
		frame.pack();
		frame.setVisible(true);
		
		
	}

	public void valueChanged(ListSelectionEvent e) {
		
		if(!e.getValueIsAdjusting())
		{
			JList clickedList = (JList) e.getSource();
			String currentTable = (String)clickedList.getSelectedValue();
			String currentSchema = (String) explorerPanel.schemaList.getSelectedItem();
			if(currentTable != null){
				System.out.println("select * from" + currentSchema + "." + currentTable);
				String query = "select * from " + currentSchema + "." + currentTable;
				try {
					dataViewer.updateDataTableFromQuery(query);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		}
	}
	

}
