package dbexplorer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class DataViewer extends JPanel{
	JTable dataTable;
	JScrollPane dataScrollPane;
	
	public DataViewer(){
		super(new GridLayout(1,1));
		//z racji tego, �e w obr�bie metody konstruktora nie ma 
		//zmiennych, kt�re nazywaj� si� tak samo jak pola kalsy
		//mo�emy dowolnie u�ywa� this.dataTable albo dataTable
		//oraz odpowiednio this.dataScrollPane oraz dataScrollPane
		this.dataTable = new JTable();
		dataTable.setPreferredScrollableViewportSize(new Dimension(500, 70));
		dataTable.setFillsViewportHeight(true);
		this.dataScrollPane = new JScrollPane(dataTable);	
		add(dataScrollPane);
		
//		dataTable.setModel(new DefaultTableModel([][],[]));
	}

	public void updateDataTableFromQuery(String query) throws SQLException {
		//tworzymy obiekt, ktory pozwala nam si� dosta� do bazy danych
		MysqlDAO dao = new MysqlDAO();  
		dao.connect(); // z pomoc� tego obiektu nawi�zujemy po��czenie
		//wykonujemy zapytanie i zapisujemy wynik do zmiennej resultSet
		ResultSet resultSet = dao.executeQuery(query); 
		//istniej�cy komponent JTable dataTable aktualizujemy o nowe dane
		dataTable.setModel(new DefaultTableModel(MysqlDAO.getDataFromResultSet(resultSet),
				MysqlDAO.getColumnsFromResultSet(resultSet)));
		//zamykamy po��czenie do bazy
		dao.close();		
	}
	
	public static void main(String[] args) throws SQLException{
		JFrame frame = new JFrame("Przegl�darka danych");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setMinimumSize(new Dimension(500, 500));
		
		DataViewer dv = new DataViewer();
		dv.updateDataTableFromQuery("select * from customer");
		
		frame.getContentPane().add(dv,BorderLayout.CENTER);
		
		//button do zmiany wawarto�ci tabeli
		JButton button = new JButton("Change Data");
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					dv.updateDataTableFromQuery("select * from film");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		frame.getContentPane().add(button, BorderLayout.PAGE_END);

		frame.pack();
		frame.setVisible(true);
	}

}