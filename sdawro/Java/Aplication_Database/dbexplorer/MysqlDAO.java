package dbexplorer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.naming.spi.DirStateFactory.Result;

public class MysqlDAO {
	private Connection connect = null; // obiekt ��cz�cy sie z baz�	
	private Statement statement = null; // obiekt przechowuj�cy zapytanie
	private ResultSet resultSet = null; // obiekt przechowuj�cy wynik zapytania

	// metoda sluzaca do nawiazywania polaczenia z lokalna baza sakila z uzycie uzytkownika root !
	
	public void connect() {
		try {
			connect = DriverManager.getConnection("jdbc:mysql://localhost/sakila?" + "user=root&password=root");
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}
// metoda, kt�ra po wykonaniu wszystkich operacji, kt�re bedziemy wykonywac w ramach zapytania
	public void close() {
		try {
			if (resultSet != null) {
				resultSet.close();
			}

			if (statement != null) {
				statement.close();
			}

			if (connect != null) {
				connect.close();
			}
		} catch (Exception e) {

		}
	}

	//metoda,kt�ra dla aktywanego po��czenia 
	//wykonuje zapytanie i zwraca wynik w result
	public ResultSet executeQuery(String query) {
		try {

			statement = connect.createStatement();
			resultSet = statement.executeQuery(query);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultSet;

	}

	// metoda pomocnicza, statyczna ktora wyciaga tablice nazw kolumn z dostarczonego ResultSet
	public static String[] getColumnsFromResultSet(ResultSet resultSet) throws SQLException {
		int noOfColumns = resultSet.getMetaData().getColumnCount();
		String[] columns = new String[noOfColumns];
		for (int i = 1; i <= noOfColumns; i++) {
			columns[i - 1] = resultSet.getMetaData().getColumnName(i);
		}

		return columns;
	}
	
	// metoda pomocnicza, statyczna ktora wyciaga tablice nazw kolumn z dostarczonego ResultSet
	public static String[][] getDataFromResultSet(ResultSet resultSet) throws SQLException {
		int noOfColumns = resultSet.getMetaData().getColumnCount();
		ArrayList<String[]> rowsList = new ArrayList<String[]>();
		while (resultSet.next()) {
			String[] currentRow = new String[noOfColumns];
			for (int i = 1; i <= noOfColumns; i++) {
				currentRow[i - 1] = resultSet.getString(i);
			}
			rowsList.add(currentRow);
		}

		return rowsList.toArray(new String[rowsList.size()][noOfColumns]);
	}

}
