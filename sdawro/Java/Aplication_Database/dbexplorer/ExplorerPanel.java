package dbexplorer;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;

public class ExplorerPanel extends JPanel implements ActionListener {
	JComboBox<String> schemaList;
	JList tableList;
public ExplorerPanel()
	{
	super(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	
	
	
	String[] schemas = getSchemas();
	schemaList = new JComboBox<String>(schemas);
	schemaList.addActionListener(this);
	//String[] tables = getTables();
	tableList  = new JList<String>();
	
	// combo box  - konfiguremy pozycje pierwszego elementu
	//wstawienie elementu
	
	c.gridx = 0; c.gridy = 0; c.anchor = c.FIRST_LINE_START;
	c.fill = GridBagConstraints.VERTICAL;
	add(schemaList, c);
	
	//schema list - konfigurujemmy gdze i jak wstawic liste tabel
	c.gridy = 1;c.fill = GridBagConstraints.BOTH;c.weighty=1;
	add(tableList, c);
	
	}




private String[] getTables(String schema) {
	
	if(schema.equals("sakila"))
	{
		String[] tables = {"customer", "film", "renta;"};
		return tables;
	}else if(schema.equals("sda")){
		String[] tables = {"user", "ticket"};
		return tables;
	}else {
		String[] tables = {"Columns", "Columns_privileges"};
		return tables;
	}
	
	
	//String[] tables = {"customer", "film", "rental"};
	//return tables;
}
private String[] getSchemas() {
	String[] schemaList = {"sakila", "sda", "information_schema"};
	return schemaList;
}
public static void main(String[] args) {
	
	JFrame frame = new JFrame("Przeglądarka danych");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	//frame.setSize(500,500);
	frame.setPreferredSize(new Dimension(500, 500));
	//frame.setMinimumSize(new Dimension(500, 500));
	
	ExplorerPanel ep = new ExplorerPanel();
	frame.setContentPane(ep);
	
	
	frame.pack();
	frame.setVisible(true);
}




@Override
public void actionPerformed(ActionEvent e) {
	
	e.getSource();
	JComboBox<String> clicedCombobox = (JComboBox<String>)e.getSource();
	String selectedSchema = (String)clicedCombobox.getSelectedItem();
	
	String[] tableArray = getTables(selectedSchema);
	tableList.setListData(tableArray);
}
	
}


