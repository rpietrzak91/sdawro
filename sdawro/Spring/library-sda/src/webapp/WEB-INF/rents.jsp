<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/header.jsp" %>
<%@ include file="/WEB-INF/include/navbar.jsp" %>

<%--<c:url value="/rent/update" var="userUpdateUrl"/>--%>
<c:url value="/rent/finish" var="finishRentUrl"/>

<div class="container">

    <h1>List of rents</h1>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="text-center col-md-1">Id</th>
                    <th class="text-center">Date</th>
                    <th class="text-center">User</th>
                    <th class="text-center col-md-1">Book</th>
                    <th class="text-center col-md-1">Status</th>
                    <th class="text-center col-md-1">Action</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${rent}" var="rent">
                    <tr>
                        <td>${rent.id}</td>
                        <td>Zapisuje do bazy danych, ale nie odczytuje :/ "$""{rent""data}" </td>
                        <td>${rent.book.user.email}</td> // ????????
                        <td>${rent.book.title}</td>
                        <td>${rent.book.availability}</td>
                        <td class="text-center">
                            <a class="btn btn-sm btn-primary" type="submit" href="${booksUrl}/${rent.id}">Zakoncz</a>
                        </td>

                    </tr>
                </c:forEach>


                </tbody>
            </table>
        </div>
    </div>

</div>
<%@ include file="/WEB-INF/include/footer.jsp" %>