<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/header.jsp" %>
<c:url value="/register" var="backRegisterUrl"></c:url>

<div class="container">
    <div class="card card-container">
        <h4> Login to your account</h4>
        <form class="form-signin" action="" method="post">
            <input type="hidden"
                   name=""
                   value=""/>
            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <div id="remember" class="checkbox">
                <label>
                    <input type="checkbox" value="remember-me"> Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
        </form>
        <div class="margin-bottom-10">
            <a href="#" class="forgot-password">Forgot the password?</a>
        </div>
        <div class="margin-bottom-10">
            Don't have account? <a href="${backRegisterUrl}" class="register">Create account</a>
        </div>
    </div>
</div>
<%@ include file="/WEB-INF/include/footer.jsp" %>














