<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/header.jsp" %>

<c:url value="/book/createbook" var="createbook"/>


<div class="container">
    <div class="card card-container">
        <h4> Create new account</h4>
        <form class="form-signin" method="post" action="${createbook}">
            <input type="text" name="title" id="inputEmail" class="form-control" placeholder="Title" required autofocus>
            <input type="text" name="author" id="inputFirstName" class="form-control" placeholder="Author" required>
            <input type="text" name="price" id="inputLastName" class="form-control" placeholder="Price" required>
            <input type="text" name="availability" id="inputPassword" class="form-control" placeholder="Availability" >


            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Create new Book</button>
        </form>
    </div>
</div>

<%@ include file="/WEB-INF/include/footer.jsp" %>