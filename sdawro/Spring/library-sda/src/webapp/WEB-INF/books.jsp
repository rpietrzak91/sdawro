<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/header.jsp" %>
<%@ include file="/WEB-INF/include/navbar.jsp" %>
<c:url value="/book/update" var="booksUrl"></c:url>
<c:url value="/book/edit" var="editUrl"></c:url>
<c:url value="/book/delete" var="deleteUrl"></c:url>
<c:url value="/book/rent" var="rentUrl"></c:url>


<div class="container">

    <h1>List of books</h1>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="text-center col-md-1">Id</th>
                    <th class="text-center">Title</th>
                    <th class="text-center">Author</th>
                    <th class="text-center col-md-1">Price</th>
                    <th class="text-center col-md-1">Availability</th>
                    <th class="text-center col-md-1">Admin</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach items="${books}" var="books">
                    <tr>

                        <td>${books.id}</td>
                        <td>${books.title}</td>
                        <td>${books.author}</td>
                        <td>${books.price}</td>
                        <td>${books.availability}</td>
                        <td class="text-center">
                        <a class="btn btn-sm btn-primary" type="submit" href="${booksUrl}/${books.id}">Edit</a>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-sm btn-danger" type="submit" href="${deleteUrl}/${books.id}">Delete</a>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-sm btn-warning" type="submit" href="${rentUrl}/${books.id}">RENT</a>
                        </td>
                    </tr>
                </c:forEach>



                </tbody>
            </table>
        </div>
    </div>

</div>
<%@ include file="/WEB-INF/include/footer.jsp" %>






















