<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/header.jsp" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

v<c:url var="userUpdateUrl" value="/book/update" />


<div class="container">
    <div class="card card-container">
        <h4> Edit book </h4>
        <form:form  class="form-signin" method="post" commandName="bookEdit" action="${userUpdateUrl}">
            <form:hidden path="id"></form:hidden>
            <form:input path="title" class="form-control" placeholder="Title"></form:input><br/>
            <form:input path="author" class="form-control" placeholder="Author"></form:input><br/>
            <form:input path="price" class="form-control" placeholder="Price"></form:input><br/>
            <form:input path="availability" class="form-control" placeholder="Availability"></form:input><br/>
            <form:label path="user.id" >Save book down user</form:label>
            <form:select items="${users}" path="user.id" itemValue="id"  class="form-control" itemLabel="email">
                <form:option value="empty">TEST</form:option>
            </form:select><br/>

            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Update book</button>
        </form:form>
    </div>
</div>

<%@ include file="/WEB-INF/include/footer.jsp" %>