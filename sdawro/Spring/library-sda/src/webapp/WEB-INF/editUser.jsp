<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/header.jsp" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

v<c:url var="saveUpdateUser" value="user/save" />


<div class="container">
    <div class="card card-container">
        <h4> Edit book </h4>
        <form:form  class="form-signin" method="post" commandName="updateUser" action="${saveUpdateUser}">
            <form:hidden path="id"></form:hidden>
            <form:input path="name" class="form-control" ></form:input><br/>
            <form:input path="lastName" class="form-control" ></form:input><br/>
            <form:input path="email" class="form-control" ></form:input><br/>
            <form:input path="password" class="form-control"></form:input><br/>
            <form:input path="repeatpassword" class="form-control"></form:input><br/>

            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Update user</button>

        </form:form>
    </div>
</div>

<%@ include file="/WEB-INF/include/footer.jsp" %>