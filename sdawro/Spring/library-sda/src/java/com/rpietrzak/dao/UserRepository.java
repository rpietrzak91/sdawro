package com.rpietrzak.dao;

import com.rpietrzak.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by RADEK on 2016-07-10.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
