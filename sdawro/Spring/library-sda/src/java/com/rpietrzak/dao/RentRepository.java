package com.rpietrzak.dao;

import com.rpietrzak.model.Book;
import com.rpietrzak.model.Rent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by RADEK on 2016-07-14.
 */
public interface RentRepository extends JpaRepository<Rent,Long > {

}
