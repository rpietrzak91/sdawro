package com.rpietrzak.dao;

import com.rpietrzak.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by RADEK on 2016-07-11.
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
}
