package com.rpietrzak.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by RADEK on 2016-07-11.
 */
@Entity
@Table(name="book")
public class Book extends BaseEntity {

    @NotEmpty
    private String title;

    @NotEmpty
    private String author;

    @NotEmpty
    private String price;

    @NotEmpty
    private String availability;

//    @NotEmpty
//    @Column(columnDefinition = "DATE DEFAULT CURRENT_DATE")
//    private Date createdDate;

    ;@ManyToOne
    @JoinColumn(name="user_id")
    private User user;


    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAvailability() {
        return availability;
    }



    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }



    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

//
//    public Date getCreatedDate() {
////        if(this.createdDate == null){
////            return new Date();
////        }
//        return createdDate;
//    }
//
//    public void setCreatedDate(Date createdDate) {
//        this.createdDate = createdDate;
//    }


}
