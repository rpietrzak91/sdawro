package com.rpietrzak.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.crypto.Data;
import java.util.Date;

/**
 * Created by RADEK on 2016-07-14.
 */
@Entity
public class Rent extends BaseEntity {




    @Column(name="create_date")
    private Date data;

    @ManyToOne
    @JoinColumn(name="user_id") // nazwy kolumy z kluczem obcym
    private User user;

    @ManyToOne
    @JoinColumn(name="book_id") // nazwy kolumy z kluczem obcym
    private Book book;

    public Rent() {

    }

    public void setCreatedDate(Date createdDate) {
        this.data = createdDate;
    }

    public void setUser(Object o) {
        this.user = user;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Date getCreatedDate() {
        return data;
    }

    public User getUser() {
        return user;
    }

    public Book getBook() {
        return book;
    }

    @Override
    public String toString() {
        return "Rent{" +
                "data=" + data +
                ", user=" + user +
                ", book=" + book +
                '}';
    }
}
