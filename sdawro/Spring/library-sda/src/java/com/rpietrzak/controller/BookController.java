package com.rpietrzak.controller;


import com.rpietrzak.dao.BookRepository;
import com.rpietrzak.dao.RentRepository;
import com.rpietrzak.dao.UserRepository;
import com.rpietrzak.model.Book;
import com.rpietrzak.model.Rent;
import com.rpietrzak.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import javax.xml.crypto.Data;


import java.util.ArrayList;

import java.util.Date;
import java.util.List;

import static org.springframework.data.jpa.domain.AbstractPersistable_.id;

/**
 * Created by RADEK on 2016-07-11.
 */
@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    BookRepository bookRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RentRepository rentRepository;

    @RequestMapping("/create")
    public String getViewCreateBook() {

        return "/create";
    }

    @RequestMapping(value = "/createbook", method = RequestMethod.POST)
    public String saveBook(@ModelAttribute @Valid Book book, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {

            return "/create";
        } else {

            bookRepository.save(book);
            return "redirect:/book/books";
        }
    }


    @RequestMapping(value = "/books", method = RequestMethod.GET)
    public String listBooks(Model model) {

        List<Book> books = bookRepository.findAll();
        model.addAttribute("books", books);

        return "/books";
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public String editBook(@PathVariable Long id, Model model) {


        Book book = bookRepository.findOne(id);
        List<User> users = userRepository.findAll();
        model.addAttribute("users", users);
        model.addAttribute("bookEdit", book);

        return "/editBook";


    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String getUpdateBook(@ModelAttribute Book book) {

        bookRepository.save(book);

        return "redirect:/book/books";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String removeBook(@PathVariable Long id) {

        bookRepository.delete(id);
        return "redirect:/book/books";
    }

    @RequestMapping(value="/rent/{id}", method = RequestMethod.GET)
    public String getViewRent(@PathVariable Long id, Model model) {

        Rent rent = new Rent();
        Book book = bookRepository.findOne(id);


        // iteracja bazy danych jest inkremetacja??
       // rent.setId(id); // ???????
        rent.setCreatedDate(new Date());
        rent.setBook(book);
        rent.setUser(book);
        rentRepository.save(rent);
         List<Rent> rents = rentRepository.findAll();

         model.addAttribute("rent", rents);


        return "/rents";
    }

}



