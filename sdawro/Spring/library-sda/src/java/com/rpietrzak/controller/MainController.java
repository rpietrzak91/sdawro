package com.rpietrzak.controller;

import com.rpietrzak.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by RADEK on 2016-07-10.
 */
@Controller
public class MainController {

    @RequestMapping(value="/", method = RequestMethod.GET)
    public String getLoginPage(Model model) {


        return "/login";
    }

    @RequestMapping("/login")
    public String getReturnLogin(){

        return "login";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String getRegisterPage(Model model){

        //User user = new User();
        //model.addAttribute("persons", user);

        return "register";
    }


}
