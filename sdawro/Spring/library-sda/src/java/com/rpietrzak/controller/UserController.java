package com.rpietrzak.controller;

import com.rpietrzak.dao.UserRepository;
import com.rpietrzak.model.Book;
import com.rpietrzak.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by RADEK on 2016-07-10.
 */
@Controller
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String createUser(@Valid @ModelAttribute User user, BindingResult bindingResult) {

             if(bindingResult.hasErrors()) {

                 return "register";

             }else {

                 userRepository.save(user);
                 return "redirect:/users";
             }
    }

    @RequestMapping("/users")
    public String getViewUsers(Model model) {

        List<User> users = userRepository.findAll();
        model.addAttribute("users", users);
        return "users";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable long id) {

        userRepository.delete(id);
        return "redirect:/users";
    }

    @RequestMapping("/user/create")
    public String getCreateUser() {

         return "/register";
    }

    @RequestMapping(value = "user/update/{id}", method = RequestMethod.GET)
    public String updateUser(@PathVariable long id, Model model) {

        User user = userRepository.findOne(id);
        model.addAttribute("updateUser", user);
        return "/editUser";
    }

    @RequestMapping(value = "/user/update/user/save", method = RequestMethod.POST)
    public String updateUserSave(@ModelAttribute User user) {

        userRepository.save(user);
        return "redirect:/users";
    }









}
